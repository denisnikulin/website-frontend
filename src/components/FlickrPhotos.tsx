import _ from "lodash";
import React, { FunctionComponent } from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  line-height: 1em;
  margin-top: 5px;
  border-radius: 5px;
  background: #ccc;
  background-clip: padding-box;
  overflow: hidden;
  height: 50px;
  position: relative;
`;

const InnerWrapper = styled.div`
  position: absolute;
  white-space: nowrap;
`;

const PhotoA = styled.a`
  display: inline-block;
  width: 50px;
  height: 50px;
  vertical-align: bottom;
  border: none !important;
  position: relative;
`;
const PhotoImg = styled.img`
  filter: grayscale(100%);
  color: #ccc;
  a:hover &,
  a:active & {
    filter: none;
  }
`;
const PhotoUnderline = styled.span`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  display: block;

  a:hover & {
    border-top: 2px solid red;
  }
`;

const FlickrPhotos: FunctionComponent<{
  info: {
    mostViewedPhotos: Array<{ title: any; url: any; thumbnailUrl: any }>;
  };
  children?: never;
}> = ({ info }) => {
  return (
    <Wrapper>
      <InnerWrapper>
        {_.map(
          _.get(info, "mostViewedPhotos"),
          ({ title, url, thumbnailUrl }, index) => (
            <PhotoA href={url} title={title} key={index}>
              <PhotoImg width="50" height="50" alt={title} src={thumbnailUrl} />
              <PhotoUnderline />
            </PhotoA>
          ),
        )}
      </InnerWrapper>
    </Wrapper>
  );
};

export default FlickrPhotos;
