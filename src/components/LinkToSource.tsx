import React, { FunctionComponent } from "react";
import styled from "styled-components";

import { Trans } from "../i18n";

const Wrapper = styled.div`
  position: absolute;
  bottom: 15px;
  right: 20px;

  @media (max-width: 600px) {
    right: 0;
    left: 0;
  }
`;

const LinkToSource: FunctionComponent<{ children?: never }> = () => {
  return (
    <Wrapper>
      <a href="https://gitlab.com/kachkaev/website">
        <Trans i18nKey="websiteSourceCode" />
      </a>
    </Wrapper>
  );
};

export default LinkToSource;
