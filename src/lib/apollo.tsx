import { getDataFromTree } from "@apollo/react-ssr";
import { InMemoryCache, NormalizedCacheObject } from "apollo-cache-inmemory";
import { ApolloClient } from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { createPersistedQueryLink } from "apollo-link-persisted-queries";
import fetch from "isomorphic-unfetch";
import * as React from "react";

export type AppApolloClient = ApolloClient<NormalizedCacheObject>;
let browserApolloClient: AppApolloClient;

const createClient = ({ uri, initialState }) => {
  const isBrowser = typeof window !== "undefined";

  return new ApolloClient({
    connectToDevTools: isBrowser,
    ssrMode: !isBrowser,
    link: createPersistedQueryLink().concat(
      new HttpLink({
        uri,
        // Use fetch() polyfill on the server
        fetch: !isBrowser ? fetch : undefined,
      }),
    ),
    cache: new InMemoryCache().restore(initialState || {}),
  });
};

const initApollo = (options) => {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (typeof window === "undefined") {
    return createClient(options);
  }

  // Reuse client on the client-side
  if (!browserApolloClient) {
    browserApolloClient = createClient(options);
  }

  return browserApolloClient;
};

export const withApolloClient = (App) => {
  return class Apollo extends React.Component {
    static displayName = "withApollo(App)";
    static async getInitialProps(ctx) {
      const { AppTree } = ctx;
      const { graphqlUri } = ctx.ctx.req || (window as any).__NEXT_DATA__.props;
      const apolloClient = initApollo({ uri: graphqlUri });
      ctx.ctx.apolloClient = apolloClient;

      let appProps = {};
      if (App.getInitialProps) {
        appProps = await App.getInitialProps(ctx);
      }

      // Run all GraphQL queries in the component tree
      // and extract the resulting data
      if (typeof window === "undefined") {
        try {
          // Run all GraphQL queries
          await getDataFromTree(
            <AppTree {...appProps} apolloClient={apolloClient} />,
          );
        } catch (error) {
          // Prevent Apollo Client GraphQL errors from crashing SSR.
          // Handle them in components via the data.error prop:
          // https://www.apollographql.com/docs/react/api/react-apollo.html#graphql-query-data-error
          if (error.message !== "Product not found") {
            // eslint-disable-next-line no-console
            console.error(
              "Error while running `getDataFromTree`",
              JSON.stringify(error),
            );
          }
        }
      }

      // Extract query data from the Apollo store
      const apolloState = apolloClient.cache.extract();

      return {
        ...appProps,
        apolloState,
        graphqlUri,
      };
    }

    private apolloClient;

    constructor(props) {
      super(props);
      this.apolloClient = initApollo({
        initialState: props.apolloState,
        uri: props.graphqlUri,
      });
    }

    render() {
      return <App apolloClient={this.apolloClient} {...this.props} />;
    }
  };
};
